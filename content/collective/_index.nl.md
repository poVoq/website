+++
title = "Over het JoinJabber-collectief"
+++

In deze sectie vind je de [notulen](https://en.wikipedia.org/wiki/Minutes) van onze vergaderingen. Als je geïnteresseerd bent in onze doelstellingen, kun je ze [hier](@/collective/about/goals/index.nl.md) vinden.
